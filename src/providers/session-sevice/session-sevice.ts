import {Injectable} from '@angular/core';
import {AuthHttp} from "angular2-jwt";
import {baseUrl} from '../../variables';


@Injectable()
export class SessionSeviceProvider {
  //baseUrl = 'http://localhost:9090';
  url = baseUrl + '/session';

  constructor(public http: AuthHttp) {
  }

  getSessionById(sessionid: String){
    return this.http.get(this.url + '/getsessionbyid/' + sessionid);
  }

  makeMove(sessionid: string, cardid: string) {
    // return this.http.get(this.url + '/makemove?sessionid=' + sessionid + '&cardid=' + cardid);
    return this.http.post(this.url + '/makemove/' + sessionid + '/' + cardid, undefined);
  }

  getAllOpenSessions() {
    return this.http.get(this.url + '/getallopensessions');
  }

  getAllPrepSessionsParticipant(){
    return this.http.get(this.url + '/getallprepsessionsparticipant');
  }

  getAllPrepSessionsOrganisator(){
    return this.http.get(this.url + '/getallprepsessionsorganisator');
  }

  endSession(sessionid: string) {
    return this.http.post(this.url + '/stopsession/' + sessionid, undefined);
  }

  getCarById(cardid: string) {
    return this.http.get(baseUrl + '/card' + '/getcardbyid/' + cardid);
  }

  getCurrentUser() {
    return this.http.get(baseUrl + '/users' + '/user');
  }

  getThemeById(themeid: string) {
    return this.http.get(baseUrl + '/theme/getthemebyid?themeid=' +  themeid);
  }

  getUsernameByUserid(userid: string) {
    return this.http.get(baseUrl + '/users' + '/username/' + userid);
  }

  getChatLog(sessionid: String) {
    return this.http.get(this.url + '/getchatlogofsession/' + sessionid);
  }

}
