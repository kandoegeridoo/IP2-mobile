import { Injectable } from '@angular/core';
import {AuthHttp} from 'angular2-jwt';
import {baseUrl} from '../../variables';


@Injectable()
export class AuthServiceProvider {
  //baseUrl = 'http://localhost:9090';
  //baseUrl = 'https://kandoegeridoo-backend.herokuapp.com';
  registerUrl = baseUrl + '/users/register';
  loginUrl = baseUrl + '/login';

  constructor(public http: AuthHttp) {
  }


  register(username: String, email: String, password: String) {
    return this.http.post(this.registerUrl, {username, email, password});
  }

  login(email: String, password: String) {
    console.log('logging in');
    return this.http.post(this.loginUrl, {email, password});
  }

}
