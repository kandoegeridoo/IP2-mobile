import {Injectable} from '@angular/core';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import {PushNotification} from "../../model/PushNotification";
import {baseUrl} from "../../variables";
import {LocalNotifications} from "@ionic-native/local-notifications";

/*
  Generated class for the NotificationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NotificationProvider {
  private serverUrl = baseUrl + '/socket';
  private stompClient;
  private userId;
  private notifyId = 1;

  constructor(private localNotifications: LocalNotifications) {
  }

  public startListening() {
    this.userId = localStorage.getItem('userId');
    this.initSockAndStomp();
  }

  private initSockAndStomp() {
    const websocket = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(websocket);
    this.stompClient.connect({}, (frame) => {
      this.stompClient.subscribe('/kandoo/notifications/' + this.userId, (message) => {
        if (message.body) {
          const notification: PushNotification = JSON.parse(message.body);
          this.localNotifications.schedule({
            id: this.notifyId++,
            title: notification.title,
            text: notification.body
          })
        }
      });
    });
  }
}
