import {NgModule} from '@angular/core';
import {SessionCardComponent} from './session-card/session-card';
import {IonicModule} from "ionic-angular";
import {IngamecardComponent} from './ingamecard/ingamecard';

@NgModule({
  declarations: [SessionCardComponent,
    IngamecardComponent],
  imports: [IonicModule],
  exports: [SessionCardComponent,
    IngamecardComponent]
})
export class ComponentsModule {
}
