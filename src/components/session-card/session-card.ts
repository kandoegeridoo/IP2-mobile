import {Component, Input, OnInit} from '@angular/core';
import {Session} from "../../model/Session";
import {App, NavController, ToastController} from "ionic-angular";
import {SessionSeviceProvider} from "../../providers/session-sevice/session-sevice";
import {GamemenuPage} from "../../pages/gamemenu/gamemenu";

@Component({
  selector: 'session-card',
  templateUrl: 'session-card.html'
})
export class SessionCardComponent implements OnInit {
  @Input() session: Session;
  theme;
  //need this user to place the star if he is the organisator
  @Input() user;
  isOrganisator = false;

  constructor(private app: App, public nav: NavController, private toastCtrl: ToastController, private sessionService: SessionSeviceProvider) {
  }

  ngOnInit(): void {
    this.sessionService.getThemeById(this.session.themeid).subscribe(data => {
      if (this.session.createdFromSubtheme == false && data.json() != null) {
        this.theme = data.json();
        for (let i = 0; i < this.theme.organisators.length; i++) {
          if (this.theme.organisators[i] === this.user.userId) {
            this.isOrganisator = true;
          }
        }
      }
    });
  }

  navigate() {
    if (this.session.started) {
      localStorage.setItem("session", JSON.stringify(this.session));
      // this.nav.push(GamemenuPage);
      this.app.getRootNav().push(GamemenuPage);
    } else {
      this.presentToast();
    }
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Deze sessie is in opmaak',
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

}
