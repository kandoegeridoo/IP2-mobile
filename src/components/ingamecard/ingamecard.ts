import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CardState} from "../../model/CardState";
import {SessionSeviceProvider} from "../../providers/session-sevice/session-sevice";
import {Card} from "../../model/Card";

@Component({
  selector: 'ingamecard',
  templateUrl: 'ingamecard.html'
})
export class IngamecardComponent implements OnInit{
  @Input() cardstate: CardState;
  @Input() sessionid: string;
  card: Card;
  @Output() cardMoved = new EventEmitter();
  @Input() index: number;

  constructor(private service: SessionSeviceProvider) {

  }

  ngOnInit(): void {
    this.service.getCarById(this.cardstate.cardid).subscribe( data => this.card = data.json());
  }

  moveCard() {
    this.service.makeMove(this.sessionid, this.card.id).subscribe(data =>
      this.cardMoved.emit()
    );
  }

  getColor() {
    if (this.index === 0) {
      return 'gold';
    } else if (this.index === 1) {
      return 'silver';
    } else if (this.index === 2) {
      return 'rgb(205, 127, 50)';
    } else {
      return 'whitesmoke';
    }
  }


}
