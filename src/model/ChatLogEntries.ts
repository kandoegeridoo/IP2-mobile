export class ChatLogEntries {
  message;
  sender;
  username;

  constructor(sender, message, username) {
    this.sender = sender;
    this.message = message;
    this.username = username;
  }
}
