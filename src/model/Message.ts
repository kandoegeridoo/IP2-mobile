export class Message {
  message: string;
  sender: string;
  username: string;
}
