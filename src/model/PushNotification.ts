export class PushNotification {
  title: string;
  body: string;
  icon: string;
}
