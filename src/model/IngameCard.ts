import {Card} from "./Card";
import {CardState} from "./CardState";

export class IngameCard {
  id: string;
  name: string;
  description: string;
  image: string;
  position: number;

  addCard(card: Card) {
    this.id = card.id;
    this.name = card.name;
    this.description = card.description;
    this.image = card.image;
  }

  addPosition(cardstate: CardState){
    if (cardstate.cardid === this.id) {
      this.position = cardstate.position;
    }
  }

}

