import {CardState} from './CardState';

export class SessionState {
  cardStates: CardState[];
  filler: string;
}
