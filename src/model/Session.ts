import {List} from './List';
import {SessionState} from './SessionState';
import {Chatlog} from "./Chatlog";


export class Session {
  id: string;
  subject: string;
  allCardids: List<string>;
  themeid: string;
  started: boolean;
  sessionStates: SessionState[];
  chatlog: Chatlog;
  userIds: string[];
  currentPlayer: string;
  ended: boolean;
  createdFromSubtheme: boolean;
}
