import {Message} from "./Message";

export class Chatlog {
  id: string;
  message: Message[];
}
