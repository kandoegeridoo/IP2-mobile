export class List<T> {
  public items: Array<T>;

  constructor() {
    this.items = [];
  }

  size(): number {
    return this.items.length;
  }

  add(value: T): void {
    this.items.push(value);
  }

  get(index: number): T {
    return this.items[index];
  }

  setFromArray(array : Array<T>): void {
    this.items = array;
  }

  setFromList(list : List<T>) : void{

    this.items = list.items;

  }

  getAsArray(): Array<T>{
    return this.items;
  }

  remove(value: T): void{
    this.items = this.items.filter(x => x !== value);

  }

}
