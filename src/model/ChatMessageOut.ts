export class ChatMessageOut {
  userId;
  content;

  constructor(userId, content) {
    this.userId = userId;
    this.content = content;
  }
}
