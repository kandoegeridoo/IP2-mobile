export class ChatMessageIn {
  username;
  content;

  constructor(username, content) {
    this.username = username;
    this.content = content;
  }
}
