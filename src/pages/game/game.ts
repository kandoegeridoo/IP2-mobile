import {Component, OnInit} from '@angular/core';
import {AlertController, App, IonicPage, NavController, NavParams} from 'ionic-angular';
import {SessionSeviceProvider} from "../../providers/session-sevice/session-sevice";
import {Session} from "../../model/Session";
import {baseUrl} from "../../variables";
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import {HomePage} from "../home/home";
import {Card} from "../../model/Card";
import {Dialogs} from "@ionic-native/dialogs";


@IonicPage()
@Component({
  selector: 'page-game',
  templateUrl: 'game.html',
})
export class GamePage implements OnInit {
  session: Session;
  userId;
  channel;
  isOrganisator = false;
  theme; //need to check if the user is organisator of this session
  private stompClient;
  private serverUrl = baseUrl + '/socket';
  currentPlayerName: string;
  gameIsOver = false;
  winningCard: Card;

  constructor(public navCtrl: NavController, public navParams: NavParams, private service: SessionSeviceProvider, private app: App, private alertCtrl: AlertController, private dialog: Dialogs) {
  }

  ngOnInit(): void {
    this.session = JSON.parse(localStorage.getItem('session'));
    this.cardMoved();
    this.userId = localStorage.getItem('userId');
    this.subToSocket();

    this.service.getThemeById(this.session.themeid).subscribe(data => {
      if (this.session.createdFromSubtheme == false && data.json() != null) {
        this.theme = data.json();
        for (let i = 0; i < this.theme.organisators.length; i++) {
          if (this.theme.organisators[i] === this.userId) {
            this.isOrganisator = true;
            console.log(this.theme.organisators[i]);
          }
        }
      }
    });

    this.service.getUsernameByUserid(this.session.currentPlayer).subscribe(data => this.currentPlayerName = data.json().username);

  }

  cardMoved() {
    this.service.getSessionById(this.session.id).subscribe(data => {
      this.session = data.json();
      this.sortData();
    });
  }

  sortData() {
    const data = this.session.sessionStates[this.session.sessionStates.length - 1].cardStates.slice();
    this.session.sessionStates[this.session.sessionStates.length - 1].cardStates = data.sort((a, b) => {
      return this.compare(a.position, b.position);
    });
  }

  compare(a, b) {
    if (a > b) {
      return -1;
    }
    if (a < b) {
      return 1;
    }
    return 0;
  }

  subToSocket() {
    this.channel = '/kandoo/session/' + this.session.id;
    const websocket = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(websocket);

    this.stompClient.connect({}, (frame) => {
      this.stompClient.subscribe(this.channel, (message) => {
        this.session = JSON.parse(message.body);
        if (this.session.ended) {
          this.dialog.alert("De organisator heeft gekozen om het spel te stoppen", "Sessie gestopt")
            .then(() => this.navCtrl.setRoot(HomePage))
            .catch(() => console.log("Error bij laden dialog"));
        } else {
          this.sortData();
          this.service.getUsernameByUserid(this.session.currentPlayer).subscribe(data => this.currentPlayerName = data.json().username);
        }
      });
    });
  }

  backButton() {
    this.app.getRootNav().setRoot(HomePage);
  }

  presentAlert() {
    let alert = this.alertCtrl.create({
      title: 'Het spel is gedaan!',
      subTitle: 'De kaart "' + this.winningCard.name + '" heeft de hoogste positie behaald!',
      buttons: [
        {
          text: 'Ga naar homepagina',
          handler: () => {
            this.navCtrl.setRoot(HomePage);
          }
        }]
    });
    alert.present();
  }

  endGame() {
    this.service.getCarById(this.session.sessionStates[this.session.sessionStates.length - 1].cardStates[this.session.sessionStates[this.session.sessionStates.length - 1].cardStates.length - 1].cardid).subscribe(
      data => {
        this.winningCard = data.json();
        this.presentAlert();
        this.service.endSession(this.session.id).subscribe();
      }
    );


  }

}
