import {Component, ViewChild} from '@angular/core';
import {App, Content, IonicPage, NavController, NavParams} from 'ionic-angular';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import {baseUrl} from "../../variables";
import {ChatMessageIn} from "../../model/ChatMessageIn";
import {ChatLogEntries} from "../../model/ChatLogEntries";
import {ChatMessageOut} from "../../model/ChatMessageOut";
import {SessionSeviceProvider} from "../../providers/session-sevice/session-sevice";
import {Session} from "../../model/Session";
import {HomePage} from "../home/home";


@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {
  @ViewChild(Content) content: Content;
  chatlog: ChatLogEntries[] = [];
  chatmessages: ChatMessageIn[] = [];
  message: String;
  canSend = false;
  userId: String;
  private serverUrl = baseUrl + '/socket';
  private stompClient;
  private session: Session;

  constructor(public navCtrl: NavController, public navParams: NavParams, private service: SessionSeviceProvider, private app: App) {
    this.session = JSON.parse(localStorage.getItem("session"));
  }

  ngOnInit(): void {
    this.userId = localStorage.getItem('userId');
    this.fetchOldMessages();
    this.initSockAndStomp();
    this.scrollToBottom();
  }

  userTyped() {
    this.canSend = this.message.length !== 0;
  }

  send() {
    const messageToSend = new ChatMessageOut(this.userId, this.message);
    this.stompClient.send('/app/send/message/' + this.session.id, {}, JSON.stringify(messageToSend));
    this.message = '';
    this.canSend = false;
  }

  fetchOldMessages() {
    this.service.getChatLog(this.session.id).subscribe(data => {
      this.chatlog = data.json();
    });
  }

  private initSockAndStomp() {
    const websocket = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(websocket);
    this.stompClient.connect({}, (frame) => {
      this.stompClient.subscribe('/kandoo/chat/' + this.session.id, (message) => {
        if (message.body) {
          this.chatmessages.push(JSON.parse(message.body));
          this.scrollToBottom()
        }
      });
    });
  }

  private scrollToBottom() {
    this.content.scrollToBottom();
  }

  backButton() {
    this.app.getRootNav().setRoot(HomePage);
  }

}
