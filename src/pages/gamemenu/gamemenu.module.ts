import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {GamemenuPage} from './gamemenu';

@NgModule({
  declarations: [
    GamemenuPage
  ],
  imports: [
    IonicPageModule.forChild(GamemenuPage)
  ]
})
export class GamemenuPageModule {
}
