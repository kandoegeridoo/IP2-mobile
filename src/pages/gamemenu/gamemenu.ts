import {Component} from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';

/**
 * Generated class for the GamemenuPage tabs.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gamemenu',
  templateUrl: 'gamemenu.html'
})
export class GamemenuPage {

  gameRoot = 'GamePage';
  playersRoot = 'PlayersPage';
  chatRoot = 'ChatPage';


  constructor(public navCtrl: NavController) {
  }

}
