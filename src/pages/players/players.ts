import {Component, OnInit} from '@angular/core';
import {App, IonicPage, NavController, NavParams} from 'ionic-angular';
import {Session} from "../../model/Session";
import {SessionSeviceProvider} from "../../providers/session-sevice/session-sevice";
import {HomePage} from "../home/home";


@IonicPage()
@Component({
  selector: 'page-players',
  templateUrl: 'players.html',
})
export class PlayersPage implements OnInit {
  session: Session;
  userId;
  users = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private service: SessionSeviceProvider, private app: App) {
  }

  ngOnInit(): void {
    this.session = JSON.parse(localStorage.getItem('session'));
    this.userId = localStorage.getItem('userId');

    for (let i = 0; i < this.session.userIds.length; i++) {
      this.service.getUsernameByUserid(this.session.userIds[i]).subscribe(data => {
        this.users.push(data.json());
      });
    }
  }

  backButton() {
    this.app.getRootNav().setRoot(HomePage);
  }

}
