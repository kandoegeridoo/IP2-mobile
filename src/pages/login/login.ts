import {Component} from '@angular/core';
import {AlertController, IonicPage, Loading, LoadingController, NavController} from 'ionic-angular';
import {AuthServiceProvider} from "../../providers/auth-service/auth-service";
import {HomePage} from "../home/home";

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  loading: Loading;
  registerCredentials = { email: '', password: '' };

  constructor(private nav: NavController, public auth: AuthServiceProvider, private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
    //this if makes sure you dont have to log in every time
    if (localStorage.getItem('token') != null) {
      this.nav.setRoot(HomePage);
    }
  }

  public login() {
    this.registerCredentials.email = this.registerCredentials.email.toLocaleLowerCase();
    this.showLoading();
    this.auth.login(this.registerCredentials.email, this.registerCredentials.password).subscribe(allowed => {
        if (allowed) {
          localStorage.setItem('token', allowed.headers.get('authorization'));
          this.nav.setRoot(HomePage);
        } else {
          this.showError("Access Denied");
        }
      },
      error => {
        this.showError(error);
      });
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  showError(text) {
    this.loading.dismiss();

    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }
}
