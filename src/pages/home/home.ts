import {Component, OnInit} from '@angular/core';
import {Loading, LoadingController, NavController} from 'ionic-angular';
import {SessionSeviceProvider} from "../../providers/session-sevice/session-sevice";
import {Session} from "../../model/Session";
import {LoginPage} from "../login/login";
import {NotificationProvider} from "../../providers/notification/notification";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {
  sessions: Session[] = [];
  sessionsInReviewOrganisator: Session[] = [];
  sessionsInReviewParticipant: Session[] = [];
  isHidden: boolean;
  user; //username, email, image, userId
  loading: Loading;

  constructor(public navCtrl: NavController,
              private sessionService: SessionSeviceProvider,
              private loadingCtrl: LoadingController,
              private notificationProvider: NotificationProvider) {
    this.isHidden = false;
  }

  ngOnInit(): void {
    this.showLoading();
    this.getAllSessions();

    this.sessionService.getCurrentUser().subscribe(user => {
      this.user = user.json();
      localStorage.setItem('userId', this.user.userId);
      this.loading.dismiss();
      this.notificationProvider.startListening();
    });
  }

  ionViewDidLoad() {
    this.getAllSessions();
  }

  hideShow() {
    this.isHidden = !this.isHidden;
  }

  logOut() {
    localStorage.removeItem('token');
    this.navCtrl.push(LoginPage);
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Loading sessions, please wait.',
      //dismissOnPageChange: true
    });
    this.loading.present();
  }

  getAllSessions() {
    this.sessionService.getAllOpenSessions().subscribe(sessions => {
      this.sessions = sessions.json();
    }, err => {
      console.log(err);
    });

    this.sessionService.getAllPrepSessionsOrganisator().subscribe(sessions => {
      console.log(sessions.json());
      this.sessionsInReviewOrganisator = sessions.json();
    }, err => {
      console.log(err);
    });

    this.sessionService.getAllPrepSessionsParticipant().subscribe(sessions => {
      console.log(sessions.json());
      this.sessionsInReviewParticipant = sessions.json();
    }, err => {
      console.log(err);
    });
  }

  doRefresh(refresher) {
    this.getAllSessions();

    setTimeout(() => {
      refresher.complete();
    }, 2000);
  }

}
