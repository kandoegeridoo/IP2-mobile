import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule, IonicPageModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {AuthServiceProvider} from '../providers/auth-service/auth-service';
import {HttpClientModule} from '@angular/common/http';
import {AUTH_PROVIDERS, AuthHttp, provideAuth} from 'angular2-jwt';
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {SessionSeviceProvider} from '../providers/session-sevice/session-sevice';
import {JWT_OPTIONS, JwtModule} from '@auth0/angular-jwt';
import {ComponentsModule} from "../components/components.module";
import {GamemenuPageModule} from "../pages/gamemenu/gamemenu.module";
import {LocalNotifications} from "@ionic-native/local-notifications";
import {NotificationProvider} from '../providers/notification/notification';
import {Dialogs} from '@ionic-native/dialogs';

export function jwtOptionsFactory(storage) {
  return {
    tokenGetter: () => {
      return storage.get('token');
    },
    whitelistedDomains: ['localhost:9090', 'https://kandoegeridoo-backend.herokuapp.com/']
  }
}

@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    IonicModule.forRoot(MyApp),
    IonicPageModule.forChild(HomePage),
    HttpClientModule,
    JwtModule.forRoot({
      jwtOptionsProvider: {
        provide: JWT_OPTIONS,
        useFactory: jwtOptionsFactory,
        deps: [Storage]
      }
    }),
    ComponentsModule,
    GamemenuPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthServiceProvider,
    AUTH_PROVIDERS,
    AuthHttp,
    provideAuth({
      headerName: 'Authorization',
      headerPrefix: '',
      tokenName: 'token',
      tokenGetter: (() => localStorage.getItem('token')),
      globalHeaders: [{'Content-Type': 'application/json'}],
      noJwtError: true
    }),
    SessionSeviceProvider,
    LocalNotifications,
    NotificationProvider,
    Dialogs
  ]
})
export class AppModule {
}
